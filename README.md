# Simple Telegram API for Rust

### Cargo

```toml
[dependencies]
stapi = { git = "https://gitlab.com/VolkMilit/stapi.git" }
```

### Example

```Rust
use stapi::Telegram;
use std::error::Error;
use std::path::PathBuf;

fn main() -> Result<(), Box<dyn Error>> {
    let stapi = Telegram::create_handle("123456789:qwertyui", "-100123456789");

    // If you want to use custom URL instead of api.telegram.org
    // use this method
    stapi.set_url("https://127.0.0.1:8080");

    // Send a message
    stapi.send_message("This is test.")?;

    // Send an document
    stapi.send_document(PathBuf::from("/home/user/image.png"))?;

    // Get last post, this works for bots only!
    println!("{:#?}", stapi.get_last_update().unwrap());

    Ok(())
}
```

### License

GPL v3.0
