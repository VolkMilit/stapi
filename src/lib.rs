use serde::Deserialize;
use anyhow::Result;
use multipart::client::lazy::Multipart;
use std::path::PathBuf;

#[derive(Deserialize, Debug, Clone)]
pub struct ChannelFrom {
    pub id: i32,
    pub is_bot: bool,
    pub first_name: String,
    pub last_name: Option<String>,
    pub username: String,
    pub language_code: String
}

#[derive(Deserialize, Debug, Clone)]
pub struct ChannelChat {
    pub id: i32,
    pub first_name: String,
    pub last_name: Option<String>,
    pub username: String,
    #[serde(rename = "type")]
    pub t_type: String
}

#[derive(Deserialize, Debug, Clone)]
pub struct ChannelPost {
    pub message_id: i16,
    pub from: ChannelFrom,
    pub chat: ChannelChat,
    pub date: i64,
    pub text: Option<String>
}

#[derive(Deserialize, Debug, Clone)]
pub struct Post {
    pub update_id: i64,
    #[serde(rename = "message")]
    pub post: ChannelPost
}

#[derive(Deserialize, Default, Debug)]
struct UpdateResponse {
    ok: bool,
    result: Vec<Post>
}

pub struct Telegram<'a> {
    key: &'a str,
    chat_id: &'a str,
    url: &'a str
}

impl<'a> Telegram<'a> {
    /// Create new telegram API handle.
    /// 
    /// ```
    /// # fn main() -> Result<()> {
    /// Telegram::create_handle("123456789:qwertyui", "-100123456789")
    ///     .send_message("This is test.")?;
    /// 
    /// # Ok(())
    /// # }
    pub fn create_handle(key: &'a str, chat_id: &'a str) -> Self {
        Telegram {
            key,
            chat_id,
            url: "https://api.telegram.org"
        }
    }

    /// Set custom telegram URL.
    /// 
    /// By default, create_handle use api.telegram.org,
    /// but you can use whatever url you want.
    pub fn set_url(&mut self, url: &'a str) {
        self.url = url;
    }

    /// Set chat id after creating handle.
    /// 
    /// Useful for bots. Use with get_last_update method,
    /// to determi chat id, like `post.chat.id`.
    pub fn set_chat_id(&mut self, chat_id: &'a str) {
        self.chat_id = chat_id;
    }

    /// Get last post string from channel.
    /// 
    /// Return Option<Post> or None if no posts found.
    pub fn get_last_update(&self) -> Option<Post> {
        let url = format!("{}/bot{}/getUpdates", self.url, self.key);

        let body = ureq::get(&url)
            .call()
            .expect("Something went wrong during getting response.")
            .into_string()
            .unwrap_or_default();

        match serde_json::from_str::<UpdateResponse>(&body) {
            Ok(response) => {
                if let Some(post) = response.result.last() {
                    return Some(post.clone());
                }
            },
            Err(err) => {
                println!("Error parsing telegram response: {err}");
            }
        };

        None
    }

    /// Send message to channel.
    pub fn send_message(&self, message: &str) -> Result<()> {
        ureq::post(&format!("{}/bot{}/sendMessage", self.url, self.key))
            .send_form(&[
                ("chat_id", &self.chat_id),
                ("text", message)
            ])?;
        
        Ok(())
    }

    /// Send message to channel.
    pub fn send_message_to_channel(&self, message: &str, chat_id: &str) -> Result<()> {
        ureq::post(&format!("{}/bot{}/sendMessage", self.url, self.key))
            .send_form(&[
                ("chat_id", &chat_id),
                ("text", message)
            ])?;
        
        Ok(())
    }

    /// Send sticker to channel.
    pub fn send_sticker(&self, sticker_id: &str) -> Result<()> {
        ureq::post(&format!("{}/bot{}/sendSticker", self.url, self.key))
            .send_form(&[
                ("chat_id", &self.chat_id),
                ("sticker", &sticker_id)
            ])?;
        
        Ok(())
    }

    /// Send sticker to channel.
    pub fn send_sticker_to_channel(&self, sticker_id: &str, chat_id: &str) -> Result<()> {
        ureq::post(&format!("{}/bot{}/sendSticker", self.url, self.key))
            .send_form(&[
                ("chat_id", &chat_id),
                ("sticker", &sticker_id)
            ])?;
        
        Ok(())
    }

    /// Send document to channel.
    pub fn send_document(&self, document_path: PathBuf) -> Result<()> {
        let mut multi = Multipart::new();
        multi.add_file("document", document_path);

        let mdata = multi.prepare().unwrap();

        let url = format!("{}/bot{}/sendDocument?chat_id={}", self.url, self.key, self.chat_id);
        let content_type = format!("multipart/form-data; boundary={}", mdata.boundary());
        ureq::post(&url)
            .set("Content-Type", &content_type)
            .send(mdata)?;

        Ok(())
    }

    /// Send photo to channel.
    pub fn send_photo(&self, photo_path: PathBuf) -> Result<()> {
        let mut multi = Multipart::new();
        multi.add_file("photo", photo_path);

        let mdata = multi.prepare().unwrap();

        let url = format!("{}/bot{}/sendPhoto?chat_id={}", self.url, self.key, self.chat_id);
        let content_type = format!("multipart/form-data; boundary={}", mdata.boundary());
        ureq::post(&url)
            .set("Content-Type", &content_type)
            .send(mdata)?;

        Ok(())
    }
}